$(function() {
	/* 左侧导航: 新闻源移动 */
	$(".feeds").sortable({ 
		delay: 2, //为防与点击事件冲突，延时2秒
        opacity: 0.35 //以透明度0.35随意拖动
    });
	/* 设置页面: 伸缩效果 */
	$("#news-feed").toggle(function() {
		$(this).removeClass("down").addClass("up");
		$(".news-feed .source").slideUp("fast");
	}, function() {
		$(this).removeClass("up").addClass("down");
		$(".news-feed .source").slideDown("fast");
	});
	/* 文章详情页: 伸缩效果 */
	if ($("article").length > 0) {
		var $header = $(".news-list >li > a");
		$header.each(function() {
			var $this = $(this);
			$this.click(function() {
				if (!($this.hasClass("markread"))) {
					$this.addClass("markread");
				}
				if ($this.hasClass("reading")) {
					$this.removeClass("reading");
					$this.parent().find("article").slideUp("fast");
				} else {
					$this.addClass("reading");
					$this.parent().siblings().find("a").removeClass("reading");
					$this.parent().siblings().find("article").slideUp("fast");
					$this.parent().find("article").slideDown("fast");
					//滑动到上面
				}
			});
		});
	}

	function getUrl() {
		
	}
});