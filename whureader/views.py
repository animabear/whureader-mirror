from django.shortcuts import render_to_response

def index(request):
	return render_to_response('index.html')

def setting(request):
	return render_to_response('settings.html')

def all_news(request):
	return render_to_response('all_news.html')

def hot_news(request):
	return render_to_response('hot_news.html')

def singel_news(request):
	return render_to_response('singel_news.html')