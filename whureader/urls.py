from django.conf.urls import patterns, include, url
from whureader.views import index, setting, all_news, hot_news, singel_news
from django.conf import settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'whureader.views.home', name='home'),
    # url(r'^whureader/', include('whureader.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    (r'^index/$', index),
    (r'^setting/$', setting),
    (r'^all/$', all_news),
    (r'^hot/$', hot_news),
    (r'^source/$', singel_news),
    url( r'^static/(?P<path>.*)$', 'django.views.static.serve',{ 'document_root': settings.STATIC_ROOT }),
   
)
